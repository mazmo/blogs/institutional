import Link from 'next/link'
import moment from 'moment';

import posts from '../posts'

const PostPreview = ({ post, prefetch }) => {
  return (
    <>
      <div className="container">
        <time dateTime={post.date}>
          {moment(post.date).format('dddd, LL')} ({moment(post.date).fromNow()})
        </time>

        <div className="titlelink">
          <Link href={post.urlPath} prefetch={prefetch}><a>{post.title}</a></Link>
        </div>

        <p className="listdiscription">{post.description}</p>

        <div className="readmore">
          <Link href={post.urlPath} prefetch={prefetch}><a>Leer más →</a></Link>
        </div>
      </div>

      <style jsx>{`
        .container {
          border-bottom: 1px solid #eaeaea;
          margin: 1.5rem 0;
        }

        time {
          font-size: 14px;
          color: #666;
          font-family: "Inter",-apple-system,BlinkMacSystemFont,"Segoe UI","Roboto","Oxygen","Ubuntu","Cantarell","Fira Sans","Droid Sans","Helvetica Neue",sans-serif;
        }

        .titlelink a {
          color: black;
          font-size: 24px;
          font-weight: bold;
          margin-top: 20px;
          margin-bottom: 0;
          text-align: left;
        }

        .titlelink a:hover {
          color: #ED4D3D;
        }

        .container .listdiscription {
          font-size: 16px;
          text-align: let;
        }

        .readmore a {
          margin: 10px 0;
          display: inline-block;
          text-decoration: none;
          letter-spacing: 0.04rem;
          font-weight: 500;
          color: #ED4D3D;
          text-transform: uppercase;
          transition: opacity 0.2s ease;
          font-size: 14px;
        }
      `}</style>
    </>
  )
}

export default function Home() {
  return (
    <>
      <h1 className="title">
        Blog de <a href="https://mazmo.net">mazmo.net</a>
      </h1>

      <p className="description">
        La red social de <span className="highlight">sexualidad libre</span>
      </p>

      <div className="postlist">
        {posts.map((post, i) => (
          <PostPreview key={i} post={post} prefetch={i < 3} />
        ))}
      </div>

      <style jsx>{`
        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: 4rem;
        }

        .title,
        .description {
          text-align: center;
        }

        .postlist {
          max-width: 800px;
          margin-top: 4rem;
        }

        @media (max-width: 600px) {
          .postlist {
            width: 100%;
          }
        }
      `}</style>
    </>
  )
}
