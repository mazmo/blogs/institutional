import moment from 'moment';

import Layout from '../components/Layout';

require('moment/locale/es');
moment.locale('es');

function MyApp({ Component, pageProps, router }) {
  const { pathname } = router
  return (
    <Layout pathname={pathname}>
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp
