import { useRouter } from 'next/router';

import Isologo from './Isologo';

const Nav = () => {
  const router = useRouter();

  const scrollToTop = () => {
    if (document.body.scrollTop !== 0 || document.documentElement.scrollTop !== 0) {
        window.scrollBy(0, -150);
        requestAnimationFrame(scrollToTop);
    }
  };

  const onLogoClick = () => {
    if (document.body.scrollTop !== 0 || document.documentElement.scrollTop !== 0) {
      scrollToTop();
    } else {
      router.push('/');
    }
  };

  return (
    <>
      <nav>
        <Isologo onClick={onLogoClick} />
      </nav>

      <style jsx>{`
        nav {
          display: flex;
          flex-direction: column;
          padding: 0 32px;
          position: sticky;
          top: 0;
          display: flex;
          justify-content: center;
          width: 100%;
          max-width: 100%;
          background-color: hsla(0,0%,100%,0.8);
          z-index: 101;
          min-height: 64px;
          transition: box-shadow .1s ease 0s;
          box-shadow: inset 0 -1px 0 0 rgba(0,0,0,0.1);
          backdrop-filter: saturate(180%) blur(5px);
        }
      `}</style>
    </>
  );
};

export default Nav;
